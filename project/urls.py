from django.conf.urls import include, url
from django.contrib import admin

from hlt_count.views import main, health


urlpatterns = [
    url(r'^$', main),
    url(r'^health$', health),
    url(r'^admin/', include(admin.site.urls)),
]
