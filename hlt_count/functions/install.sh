# To be executed on a virtual machine set up at: https://openstack.cern.ch/
# The whole repository should be cloned in: /home/cms_hlt/

# Installing prerequisites
yum groupinstall -y development
yum install -y zlib-dev openssl-devel sqlite-devel bzip2-devel ncurses-devel readline-devel gdbm-devel dl libbsd-devel tk-devel db4-devel mysql mysql-devel MySQL-python libaio-devel

# Installing python
PROJECT_DIR="/home/cms_hlt"
TOOLS_DIR="$PROJECT_DIR/tools"
cd $TOOLS_DIR

# Installing pip
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py

# Installing python dependencies
pip install cx_Oracle==6.0.1 MySQL-python==1.2.5
wget ftp://ftp.icm.edu.pl/vol/rzm6/linux-slc/centos/7.0.1406/cernonly/x86_64/Packages/oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm
rpm -ivh oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm
echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/oracle/12.1/client64/lib/" >> /etc/profile
scp nbartosi@lxplus021:/etc/tnsnames.ora /etc/
