#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
from __future__ import unicode_literals

import os
import sys
import time
import re

import cx_Oracle
import string

def write(json_raw,r,minLSbyPS,maxLSbyPS):
#   print r,minLSbyPS,maxLSbyPS
   minLS = -1
   maxLS = -1
#    print r, json_raw[str(r)]
   first=True

   for i in range(0,len(json_raw[str(r)])):
      if minLSbyPS > json_raw[str(r)][i][1]: continue
      else:
         if minLSbyPS < json_raw[str(r)][i][0]:
            minLS=json_raw[str(r)][i][0]
#            print 'json_raw[str(r)][i][0]',json_raw[str(r)][i][0]
         else:
            minLS=minLSbyPS
#            print 'minLSbyPS',minLSbyPS
      if maxLSbyPS < json_raw[str(r)][i][0]: continue 
      else:
         if maxLSbyPS > json_raw[str(r)][i][1]:
#            print 'json_raw[str(r)][i][1]',json_raw[str(r)][i][1]
            maxLS=json_raw[str(r)][i][1]
         else:
            maxLS=maxLSbyPS
#      print 'minLSbyPS',minLSbyPS,'maxLSbyPS',maxLSbyPS

   string = (',' if not first else ''),'"',str(r),'": [',
   print "".join(string),

   for i in range(0,len(json_raw[str(r)])):
      if minLSbyPS > json_raw[str(r)][i][1]: continue
      else:
         if minLSbyPS < json_raw[str(r)][i][0]:
            minLS=json_raw[str(r)][i][0]
#            print 'json_raw[str(r)][i][0]',json_raw[str(r)][i][0]
         else:
            minLS=minLSbyPS
#            print 'minLSbyPS',minLSbyPS
      if maxLSbyPS < json_raw[str(r)][i][0]: continue 
      else:
         if maxLSbyPS > json_raw[str(r)][i][1]:
#            print 'json_raw[str(r)][i][1]',json_raw[str(r)][i][1]
            maxLS=json_raw[str(r)][i][1]
         else:
#            print 'maxLSbyPS',maxLSbyPS
            maxLS=maxLSbyPS

      string = (',' if not first else ''),'[',str(minLS),',',str(maxLS),']'
      print "".join(string),
      first=False
      minLS=-1
      maxLS=-1

      print ']',


#json_DCS = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/DCSOnly/json_DCSONLY.txt" # 2016
json_DCS = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/DCSOnly/json_DCSONLY.txt" # 2017
json_GOLDEN = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/Cert_271036-279931_13TeV_PromptReco_Collisions16_JSON_NoL1T.txt"

connstr='cms_trg_r/X3lmdvu4@cms_orcon_adg'
conn = cx_Oracle.connect(connstr)
curs = conn.cursor()
curs.arraysize=50

if len(sys.argv)<3 :
   print "Usage:",sys.argv[0]," HLTkey PStitle [json]"
   exit(0)

HLTkeys=[]
if (sys.argv[1]).find(","):
   tmp = (sys.argv[1]).split(",")
   from collections import OrderedDict
   HLTkeys=list(OrderedDict.fromkeys(tmp))
else:
   HLTkeys=sys.argv[1]
#print HLTkeys

PS_TITLE=sys.argv[2]

if len(sys.argv)==4:
   jsonFILE = sys.argv[3]
else:
   jsonFILE = json_DCS

import json,ast
json_raw = json.load(open(jsonFILE))
json_raw = ast.literal_eval(json.dumps(json.load(open(jsonFILE))))
json_raw_dump = json.dumps(json.load(open(jsonFILE)))
#print json_raw

PSidx = -1
PS={}
PS_IDX = {}

for HLTkey in HLTkeys:
   PS_perKEY={}
   PS_IDX_perKEY = -2

   PS_IDX[HLTkey]=-2
#   print HLTkey

   query="with pq as (SELECT J.ID, J.NAME, trim('{' from trim('}' from J.VALUE)) as PRESCALE_INDEX FROM CMS_HLT_GDR.U_CONFVERSIONS A, CMS_HLT_GDR.U_CONF2SRV S, CMS_HLT_GDR.U_SRVELEMENTS J WHERE A.NAME='"+HLTkey+"' AND A.ID=S.ID_CONFVER AND J.NAME='lvl1Labels' AND J.ID_SERVICE=S.ID_SERVICE) select myindex, regexp_substr (prescale_index, '[^,]+', 1, rn) pslabel from pq cross join (select rownum rn, mod(rownum -1, level) MYINDEX from (select max (length (regexp_replace (prescale_index, '[^,]+'))) + 1 mx from pq ) connect by level <= mx ) where regexp_substr (prescale_index, '[^,]+', 1, rn) is not null order by myindex"
#   print query
   curs.execute(query)

   for rows in curs:
#      print rows
      PSidx   = rows[0]  
      PStitle = rows[1].split('"')[1]
#      print 'PSidx',PSidx,'PStitle',PStitle
#      print 'len(PS.items()):',len(PS.items())
      if len(PS.items()) > 0:
         for key,value in PS.items():
#            print '(key,PSidx',key,PSidx
            if key==rows[0]:
#               print '--->',value, PStitle
               if PStitle != value:
                  print 'ERROR'

      PS[PSidx] = PStitle
      PS_perKEY[PSidx] = PStitle
#      print 'PStitle',PStitle,'PS_TITLE',PS_TITLE
      if PStitle == PS_TITLE:
         PS_IDX_perKEY = PSidx

   if PS_IDX_perKEY < 0:
      print 'available PS title [',HLTkey,']:'
      curs.execute(query)
      for rows in curs:
         print rows
         PStitle = rows[1].split('"')[1]
         print PStitle,
      print ""
   else:
      PS_IDX[HLTkey]=PS_IDX_perKEY
#      exit(0)    

#print PS_TITLE,'-->',PS_IDX,'(PS_IDXm1',PS_IDXm1,PS[PS_IDXm1],'PS_IDXp1',PS_IDXp1,PS[PS_IDXp1],')'

first=True
print '{',
for HLTkey in HLTkeys:
#   print 'PS_IDX[',HLTkey,']:',PS_IDX[HLTkey]
   if PS_IDX[HLTkey] < 0: continue

   PS_IDXm1 = PS_IDX[HLTkey]-1
   PS_IDXp1 = PS_IDX[HLTkey]+1

#   print 'PS_IDXm1',PS_IDXm1
#   print 'PS_IDXp1',PS_IDXp1

#   query="select runnum,lsnum from cms_lumi_prod.online_result_4 where runnum in (select runnumber from cms_wbm.runsummary a, cms_hlt_gdr.u_confversions b where a.hltkey=b.configid and b.name='"+HLTkey+"') order by runnum,lsnum" # 2016
   query="select runnum,lsnum from cms_lumi_prod.online_result_5 where runnum in (select runnumber from cms_wbm.runsummary a, cms_hlt_gdr.u_confversions b where a.hltkey=b.configid and b.name='"+HLTkey+"') order by runnum,lsnum" # 2017
#print query

   curs.execute(query)
   
   RUN_list=[]
   prerun=0
   for rows in curs:
#      print rows
      run = rows[0]
      if run==prerun: continue
      else: 
         prerun = run
#         print run
         RUN_list.append(run)

   for r in RUN_list:
      if str(r) not in json_raw:
#         print r,"is not available ... SKIP IT"
         continue


#########
#      string = (',' if not first else ''),'"',str(r),'": [',
#      print "".join(string),

      query="select lumi_section,idx from (select lumi_section,decode (((lag(prescale_index,1,-1) over (order by lumi_section))-prescale_index), 0 ,-1 , prescale_index) as idx from CMS_UGT_MON.VIEW_LUMI_SECTIONS where run_number="+str(r)+") where idx!=-1"
#print query
      curs.execute(query)

      PSfound=False
      irow = -1
      firstPSrow=-9
      minLS=-9
      maxLS=9999999
      for rows in curs:
         irow+=1
 #         print rows,len(rows)
         ls = rows[0]
         PSid = rows[1]
#         print 'run',r,'ls',ls,'PSid',PSid,'-->',PS[PSid]
         if not PSfound:
            if PSid != PS_IDX[HLTkey]: continue
            else:
               minLS=ls
               PSfound=True
               firstPSrow=irow
         else:
            if irow==(firstPSrow+1):
               maxLS=ls-1

         if PSfound and maxLS<9999999:
#            print 'minLS',minLS,'maxLS',maxLS
            write(json_raw,r,minLS,maxLS)
            PSfound=False
            maxLS=9999999
            minLS=-9

      if minLS > 0:
#         print 'minLS',minLS,'maxLS',maxLS
         write(json_raw,r,minLS,maxLS)
         PSfound=False
         maxLS=9999999

      first=False

print '}'

