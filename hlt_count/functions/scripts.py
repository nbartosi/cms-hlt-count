import cx_Oracle
import MySQLdb
import socket
import re


def chunks(lst, n):
    """Yield successive n-sized chunks from list"""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

# A class that interacts with the HLT's oracle database and fetches information that we need
class DBParser:

    LS_DURATION = 23.31041  # duration of a lumisection in seconds
    LAST_ENTRY = {'run': 300280}
    RE = {
        # Regex selecting names of HLT paths for which rates should be evaluated
        'hlt_names_to_store': 'HLT_(ZeroBias|Physics)(_part\d+){0,1}_v\d+',
    }

    def __init__(self) :
        # Connect to the Database
        hostname = socket.gethostname()
        # Setting up connection to the Oracle DB
        dsn = 'cms_omds_lb' #online
        if hostname.find('lxplus') > -1: dsn = 'cms_omds_adg' #offline
        self.CN_cms = cx_Oracle.connect(
            user='cms_trg_r',
            password='X3lmdvu4',
            dsn=dsn)
        self.CR_cms = self.CN_cms.cursor()
        # Setting up connection to the website MySQL DB
        self.CN_web = MySQLdb.connect(
            user='hlt_script',
            passwd='hlt_script',
            host='itrac5218',
            port=5544,
            db='web_data')
        self.CR_web = self.CN_web.cursor()
        self.hlt_names = {}

    # Key version stripper
    def strip_version(name):
        if re.match('.*_v[0-9]+', name):
            return name[:name.rfind('_')]
        return name

    # Use: Get the prescaled rate as a function 
    # Parameters: runNumber: the number of the run that we want data for
    # Returns: A dictionary [ triggerName ] [ LS ] <prescaled rate> 
    def get_rates(self):
        """Get the prescaled rate
        Note: we find the raw rate by dividing CMS_RUNINFO.HLT_SUPERVISOR_TRIGGERPATHS.Accept by 23.31041
        """
        # Getting the last entry stored in the web DB
        last_entry = self.get_last_checked_runlumi()
        # Getting entries from next lumisections of the same run
        query_base =  """
                SELECT 
                    R.PATHID,
                    R.RUNNUMBER,
                    SUM(R.PACCEPT)
                FROM
                    CMS_RUNINFO.HLT_SUPERVISOR_TRIGGERPATHS R,
                    CMS_HLT_GDR.U_PATHIDS I,
                    CMS_HLT_GDR.U_PATHS P
                WHERE
                    R.PATHID = I.PATHID AND P.ID = I.ID_PATH AND REGEXP_LIKE (P.NAME, '{0:s}') AND RUN_SELECTION_PLACEHOLDER
                GROUP BY R.PATHID, R.RUNNUMBER
                """.format(self.RE['hlt_names_to_store'])
        query = query_base.replace('RUN_SELECTION_PLACEHOLDER', 'R.RUNNUMBER = {0:d}'.format(last_entry['run']))
        self.CR_cms.execute(query)
        rates = self.CR_cms.fetchall()
        if rates:
            # Updating rate of each HLT path in this run
            print('Updating {0:d} entries in the rates table for run {1:d}'.format(len(rates), last_entry['run']))
            for rate in rates:
                query_update = """
                    UPDATE
                        hlt_count_hltcount
                    SET
                        rate = {2:d}
                    WHERE
                        hlt_path_id = {0:d} AND run = {1:d}
                    """.format(rate[0], rate[1], rate[2])
                self.CR_web.execute(query_update)
            print('+ Done updating')

        # Getting entries from next runs
        query = query_base.replace('RUN_SELECTION_PLACEHOLDER', 'R.RUNNUMBER > {0:d}'.format(last_entry['run']))
        self.CR_cms.execute(query)
        rates = self.CR_cms.fetchall()

        if not rates:
            print('No new entries found since (run: {0:d})'.format(last_entry['run']))
            return None

        # Updating HLT path names for all ids in the rates
        self.update_hlt_names(rates)

        # Writing rates to the web DB
        query = """
            INSERT INTO
                hlt_count_hltcount
                (hlt_path_id, run, rate)
            VALUES
                (%s, %s, %s)
            """
        print('Writing {0:d} entries to the rates table'.format(len(rates)))
        n_written = self.CR_web.executemany(query, rates)
        self.CN_web.commit()
        print('+ Done writing')

        return rates

    def get_hlt_id_names(self, ids=None):
        """Returns a dictionary of HLT path id-name pairs"""
        if not ids:
            # Reading all path names stored in the DB
            self.CR_web.execute('SELECT id, name FROM hlt_count_hltpath')
        else:
            # Reading names only for paths that might be in the list of relevant ids
            self.CR_web.execute('SELECT id, name FROM hlt_count_hltpath WHERE id >= {0:d} AND id <= {1:d}'.format(min(ids), max(ids)))
        data = self.CR_web.fetchall()
        return {entry[0]: entry[1] for entry in data}

    def update_hlt_names(self, entries):
        """Update the list of HLT path names"""
        ids = set([entry[0] for entry in entries])
        self.hlt_names = self.get_hlt_id_names(ids)
        unknown_ids = sorted(ids - set(self.hlt_names.keys()))
        self.fill_hlt_names(unknown_ids)
        self.hlt_names = self.get_hlt_id_names()

    def get_last_checked_runlumi(self):
        """Returns the latest run:lumisection that was checked and written to the web DB"""
        self.CR_web.execute('SELECT DISTINCT run FROM hlt_count_hltcount ORDER BY run DESC LIMIT 1')
        result = self.CR_web.fetchone()
        if not result:
            return self.LAST_ENTRY
        return {'run': result[0]}

    def fill_hlt_names(self, ids, batch_size=300):
        """Writes names of paths with the specified IDs to the web DB"""
        for chunk_ids in chunks(ids, batch_size):
            # Getting names for the given path IDs
            query = """
                SELECT
                    I.PATHID, P.NAME
                FROM
                    CMS_HLT_GDR.U_PATHS P,
                    CMS_HLT_GDR.U_PATHIDS I
                WHERE
                    I.PATHID IN ({0:s}) AND
                    P.ID=I.ID_PATH
                """.format(', '.join((str(i) for i in chunk_ids)))
            self.CR_cms.execute(query)
            data = self.CR_cms.fetchall()
            if not data:
                raise ValueError('No names found for {0:d} path ids'.format(len(chunk_ids)))
            # Writing names to the website DB
            query = """
                INSERT INTO
                    hlt_count_hltpath
                    (id, name)
                VALUES
                    (%s, %s)
                """
            print('Writing names of {0:d} HLT paths'.format(len(data)))
            n_written = self.CR_web.executemany(query, data)
            self.CN_web.commit()
            print('+ Done writing')


parser = DBParser()
parser.get_rates()
