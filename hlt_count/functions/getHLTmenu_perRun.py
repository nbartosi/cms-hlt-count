import os
import sys
import time
import re

import cx_Oracle
import string


#json_DCS = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/DCSOnly/json_DCSONLY.txt" # 2016
json_DCS = "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/DCSOnly/json_DCSONLY.txt" # 2017
json_DCS_TEST = "./json.txt"

run_min = 296785 #294927 #294645
run_max = 298000

hltPATH = "'HLT_ZeroBias_v*'"
#hltMENU = "firstCollisions17"
hltMENU = "Run2017/2e34"

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-j", dest="json",     default=None,    type="string", help="input json file for brilcalc")
parser.add_option("-r", dest="run_min",  default=None,    type="int",    help="run number (min)")
parser.add_option("-R", dest="run_max",  default=None,    type="int",    help="run number (max)")
parser.add_option("-p", dest="hlt_path", default=hltPATH, type="string", help="hlt path")
parser.add_option("-m", dest="hlt_menu", default=None,    type="string", help="HLT menu")
(options, args) = parser.parse_args()

if options.json == None:
    json = json_DCS
else:
    json = options.json
if options.run_min != None:
    run_min = options.run_min
if options.run_max != None:
    run_max = options.run_max
hltPATH = options.hlt_path
if options.hlt_menu != None:
    hltMENU = options.hlt_menu

print 'json',json
print 'run_min',run_min    
print 'run_max',run_max    
print 'hltPATH',hltPATH
print 'hltMENU',hltMENU

if run_max < run_min:
    print "run_min (",run_min,") > run_max (",run_max,") !!"

import commands
BRILCALC_COMMAND="brilcalc lumi -b 'STABLE BEAMS' --hltpath "+ str(hltPATH) +" -i "+ json + " --without-checkjson"
#print BRILCALC_COMMAND
out = commands.getstatusoutput(BRILCALC_COMMAND)
if out[0] != 0:
    print 'issue w/ brilcalc'
    exit()

out_split_lines = out[1].splitlines()
#print out_split_lines

input_list = []
for line in out_split_lines:
    if "HLT_" in str(line) and ':' in str(line):
        out_split = re.split(' | ',line)
#        print out_split
        run_fill = re.split(':',out_split[1])
        for i in out_split:
            if len(i) == 0: continue
            if str(i) == '|':
                continue
            else:
                lumi = i
        input_map = {}
        input_map['run']  = run_fill[0]
        input_map['fill'] = run_fill[1]
        input_map['lumi'] = lumi
#        print input_map
        input_list.append(input_map)


#DB
connstr='cms_runinfo_r/mickey2mouse@cms_orcon_adg'
connstr='cms_trg_r/X3lmdvu4@cms_orcon_adg'
conn = cx_Oracle.connect(connstr)
curs = conn.cursor()
curs.arraysize=50

out_blob = []
for i in input_list:
    run  = i['run']
    fill = i['fill']
    lumi = i['lumi']

    if int(run) < int(run_min) or int(run) > int(run_max):
        continue
    
# USUFUL : "brilcalc trg -r '+ run +' --prescale --hltpath " + str(hltpath)
    BRILCALC_COMMAND="brilcalc trg -r "+str(run)+" | grep " + str(run) + " | grep " + str(hltMENU)
#    print BRILCALC_COMMAND
    out = commands.getstatusoutput(BRILCALC_COMMAND)
#    print out
    if out[0] != 0:
        print 'issue w/ brilcalc'
        continue
    out_split = re.split(' |',out[1])
#    print out_split
    hlt_key = out_split[7]

    query='select b.L1_MENU  from CMS_WBM.RUNSUMMARY a,  CMS_TRG_L1_CONF.UGT_KEYS b where a.runnumber='+run+' and b.id=a.GTKEY'
#    print query

    curs.execute(query)
    ii=0

    curs_copy=curs
#    print curs_copy

    L1menu = ""
    for rows in curs_copy:
        L1menu = rows[0]
        ii=ii+1

    print run, hlt_key,L1menu,fill,lumi,"[/ub]"

    blob = {}
    blob['run']    = run
    blob['key']    = hlt_key
    blob['lumi']   = lumi
    blob['L1menu'] = L1menu
#    return blob
