# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HltCount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('run', models.PositiveIntegerField()),
                ('lumisection', models.PositiveSmallIntegerField()),
                ('rate', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='HltPath',
            fields=[
                ('id', models.PositiveIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='hltcount',
            name='hlt_path',
            field=models.ForeignKey(to='hlt_count.HltPath'),
        ),
    ]
