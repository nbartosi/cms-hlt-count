from django.db import models

# Create your models here.
class HltPath(models.Model):
	"""Representation of an HLT path with its name"""
	id = models.PositiveIntegerField(primary_key=True)
	name = models.CharField(max_length=255)

class HltCount(models.Model):
	"""Count of events recorded by HLT path"""
	run = models.PositiveIntegerField()
	# lumisection = models.PositiveSmallIntegerField()
	hlt_path = models.ForeignKey(HltPath)
	rate = models.PositiveIntegerField(default=0)