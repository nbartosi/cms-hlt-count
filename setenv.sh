#!/bin/bash

PROJECT_DIR=$PWD

# Activating the Virtual Python Environment
source "$PROJECT_DIR/../bin/activate"

# Installing Python dependencies
pip install -q -r $PROJECT_DIR/requirements.txt

export DJANGO_ENVIRONMENT='development'